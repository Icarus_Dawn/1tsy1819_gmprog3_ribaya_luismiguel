﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public enum EnemyState
{
    Idle,
    Attack,
    Chase,
    Patrol,
    Death,
}

public class Enemy : MonoBehaviour
{
    [SerializeField] EnemyState curState;

    private Combat enemyCombat;
    private Stats myStats;
    private Animator charAnimator;
    private NavMeshAgent charNavMeshAgent;
    private GameObject player;
    private bool moving, attacking, dead, patrolling, gotPosition;
    private float randomPointDistance;

    public GameObject dropItem, dropGold;
    public Image enemyHPBar;
    public GameObject myMarker;
    public float patrolRadius, timer = 5, attackRadius = 2, detectRadius = 4;
    
    NavMeshHit hit;
    Transform target;

    void Start()
    {
        myStats = this.GetComponent<Stats>();
        enemyCombat = GetComponent<Combat>();
        player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
        charAnimator = GetComponent<Animator>();
        charNavMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        enemyHPBar.fillAmount = myStats.currentHP / 100;

        timer -= Time.deltaTime;                    
        switch (curState)
        {
            case EnemyState.Idle: IdleUpdate(); break;
            case EnemyState.Patrol: PatrolUpdate(); break;
            case EnemyState.Attack: AttackUpdate(); break;
            case EnemyState.Death: DeathUpdate(); break;
            case EnemyState.Chase: ChaseUpdate(); break;
        }

        charAnimator.SetBool("isRunning", moving);
        charAnimator.SetBool("isWalking", patrolling);
        charAnimator.SetBool("isAttacking", attacking);
        charAnimator.SetBool("isDead", dead);

        if (myStats.currentHP <= 0)
        {
            curState = EnemyState.Death;
        }
    }

    void FindRandomPos(GameObject origin)
    {
        float nextPointX = Random.Range(origin.transform.position.x + patrolRadius, transform.position.x - patrolRadius);
        float nextPointZ = Random.Range(origin.transform.position.z + patrolRadius, transform.position.z - patrolRadius);

        Vector3 randPos = new Vector3(nextPointX, origin.transform.position.y, nextPointZ);

        if (NavMesh.SamplePosition(randPos, out hit, 0.5f, NavMesh.AllAreas))
        {
            charNavMeshAgent.SetDestination(hit.position);
        }

        gotPosition = true;
    }

    void DropItem()
    {
        int random = Random.Range(1, 100);

        if (random >= 1 && random <= 50)
        {
            Instantiate(dropItem, this.transform.position, this.transform.rotation);
        }
        else if (random >= 51 && random <= 100)
        {
            Instantiate(dropGold, this.transform.position, this.transform.rotation);
        }
    }

    void IdleUpdate()
    {
        //Debug.Log("Idle");
        charNavMeshAgent.ResetPath();
        float targetDistance = Vector3.Distance(target.position, transform.position);
        moving = false;
        attacking = false;
        patrolling = false;

        //add timer count if idle at the limit
        //switch patrol
        if (timer <= 0)
        {
            gotPosition = false;
            curState = EnemyState.Patrol;
        }

        //if i detect a player within my range i will switch to
        // attack state
        if (targetDistance <= detectRadius)
        {
            transform.LookAt(target);
            curState = EnemyState.Chase;
        }
    }

    void PatrolUpdate()
    {
        //Debug.Log("Patrolling");
        patrolling = true;
        moving = false;
        attacking = false;
        dead = false;
        charNavMeshAgent.speed = 3;

        if (gotPosition == false)
        {
            FindRandomPos(this.gameObject);
        }

        float targetDistance = Vector3.Distance(target.position, transform.position);

        //Debug.Log(randomPointDistance);
        //Debug.Log(charNavMeshAgent.stoppingDistance);

        randomPointDistance = Vector3.Distance(this.transform.position, hit.position);

        if (targetDistance <= detectRadius)
        {
            transform.LookAt(target);
            curState = EnemyState.Chase;
        }
        
        if (randomPointDistance <= charNavMeshAgent.stoppingDistance || timer <= -10) 
        {
            timer = 5;
            curState = EnemyState.Idle;
        }
    }

    void AttackUpdate()
    {
        //Debug.Log("Attacking");      
        this.transform.LookAt(player.transform.position);
        moving = false;
        float targetDistance = Vector3.Distance(target.position, transform.position);
        patrolling = false;
        dead = false;

        Stats targetStats = player.GetComponent<Stats>();
    
        if (targetStats != null)
        {    
            attacking = true;
            enemyCombat.CharCombat(targetStats);
        }

        if (targetDistance > charNavMeshAgent.stoppingDistance)
        {           
            curState = EnemyState.Chase;
        }

        if (myStats.currentHP <= 0)
        {
            curState = EnemyState.Death;
        }

        // if you have detected a enemy get the position of the
        // target and move closer to the targer
        // if min distance is enough perform a attack sequence for anim

        // if  player/enemy is out of range switch idle
    }

    void ChaseUpdate()
    {
        //Debug.Log("Chasing");
        charNavMeshAgent.speed = 5;
        float targetDistance = Vector3.Distance(target.position, transform.position);
        moving = true;
        attacking = false;
        patrolling = false;
        dead = false;
        charNavMeshAgent.SetDestination(target.position + Vector3.forward);

        if(targetDistance >= detectRadius)
        {
            timer = 5;
            charNavMeshAgent.ResetPath();
            curState = EnemyState.Idle;
        }

        if (targetDistance <= charNavMeshAgent.stoppingDistance)
        {
            transform.LookAt(target);
            curState = EnemyState.Attack;
        }
    }

    void DeathUpdate()
    {
        moving = false;
        attacking = false;
        patrolling = false;
        dead = true;
    
        DropItem();
        Destroy(this.gameObject);
 
    }

    void OnDrawGizmosSelected()
    {
       Gizmos.color = new Color(1, 0, 0, 0.5f);
       Gizmos.DrawSphere(this.transform.position, detectRadius);
    }
}
