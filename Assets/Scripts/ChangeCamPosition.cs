﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeCamPosition : MonoBehaviour
{
    public GameObject currentCamera, newCameraPos;

	// Use this for initialization
	void Start ()
    {
        Button currentButton = GetComponent<Button>();    
        currentButton.onClick.AddListener(ChangePosition);
    }
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    void ChangePosition()
    {
        currentCamera.transform.position = newCameraPos.transform.position;
    }
}
