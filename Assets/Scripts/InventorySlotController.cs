﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlotController : MonoBehaviour
{
    public Image preview;

    [SerializeField]Item item;

    public void AddNewItem(Item newItem)
    {
        item = newItem;

        preview.sprite = item.icon;
        preview.enabled = true;
    }

    public void clearItem()
    {
        item = null;
        preview.enabled = false;
        preview.sprite = null;
    }

    public void UseItem()
    {
        if (item != null)
        {
            item.Use();
            clearItem();
        }
    }
}
