﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    new public string name = "New";
    public string description = "default";
    public Sprite icon;

    public virtual void Use()
    {
        Debug.Log("USED: " + name);
    }
}
