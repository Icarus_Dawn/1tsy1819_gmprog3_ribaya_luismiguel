﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;
    private Vector3 offset;
    public float speed = 0.200f;

    void Start()
    {
        offset = this.transform.position - player.transform.position;
    }
    void LateUpdate ()
    {
        Vector3 targetPos = player.transform.position + offset;
        Vector3 smoothMovement = Vector3.Lerp(this.transform.position, targetPos, speed*Time.deltaTime);
        this.transform.position = smoothMovement;
	}
}
