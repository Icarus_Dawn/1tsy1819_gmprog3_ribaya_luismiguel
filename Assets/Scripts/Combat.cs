﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : MonoBehaviour
{
    Stats currentStats;
    public float attackDelay;
    private float attackTimer;
    Stats curTarget;

	// Use this for initialization
	void Start ()
    {
        attackTimer = attackDelay;
        currentStats = GetComponent<Stats>();
	}

    private void Update()
    {
        attackTimer -= Time.deltaTime;
    }

    public void CharCombat(Stats targetStats)
    {
        curTarget = targetStats;
    }

    void DamageTo()
    {
        curTarget.Damage(currentStats.currentDAM);
        /*Debug.Log("target health: " + curTarget.currentHP);
        Debug.Log(currentStats.currentDAM);*/
    }


}
