﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public float currentHP { get; private set; }
    public float currentDAM { get; private set; }
    public float VIT, STR;
    public int gold;

    void Awake ()
    {
        UpdateStats();
	}

    public void Damage(float dmg)
    {
        currentHP -= dmg;
    }

    public void Heal(float health)
    {
        currentHP += health;
    }

    public void UpdateStats()
    {
        currentHP = VIT * 10;

        currentDAM = STR * 10;
    }
}
