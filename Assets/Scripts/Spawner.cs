﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour 
{
    private float pointX, pointZ;
    private GameObject[] enemies;
    private Vector3 randSpawn;
    private GameObject toSpawn;

    public bool underSpawn;
    public int spawnLimit, spawnCount;
    public float spawnRadius;
    public GameObject mob1, mob2, mob3;

    NavMeshHit hit;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawSphere(this.transform.position, spawnRadius);
    }

    // Update is called once per frame
    void Update ()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (enemies.Length < spawnLimit)
        {
            underSpawn = true;
        }
        else
        {
            underSpawn = false;
        }

        if (underSpawn == true)
        {
            Spawn();
        }
    }

    public void Spawn()
    {
        int randomizer = Random.Range(1, 4);

        switch(randomizer)
        {
            case 1:
                toSpawn = mob1;
                break;
            case 2:
                toSpawn = mob2;
                break;
            case 3:
                toSpawn = mob3;
                break;
            default:
                toSpawn = null;
                break;
        }

        pointX = Random.Range(this.transform.position.x + spawnRadius, this.transform.position.x - spawnRadius);
        pointZ = Random.Range(this.transform.position.z + spawnRadius, this.transform.position.z - spawnRadius);

        randSpawn = new Vector3(pointX, this.transform.position.y, pointZ);

        if (NavMesh.SamplePosition(randSpawn, out hit, 0.5f, NavMesh.AllAreas))
        {
            Instantiate(toSpawn, hit.position, this.transform.rotation);
            spawnCount++;
        }
    }
}
