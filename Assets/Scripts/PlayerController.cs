﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public Text level, 
                exp,
                hp, 
                attack, 
                str, 
                vit, 
                playerName, 
                points, 
                gold;

    public InteractableObj target;
    public Image HPBar, exBar;
    public Button addToVIT, addToSTR;
    public PlayerMovement playerMovement;
    public GameObject statsPanel;

    private Stats myStats, targetStats;
    private Combat playerCombat;
    private NavMeshAgent playerAgent;
    private Animator playerAnimator;
    private GameObject currentTarget;
    private Enemy enemyScript;

    public float targetDistance , exPoints, maxExPoints = 100;
    public int currentLevel = 1, unallocatedPoints = 0;
    public bool levellingUp, hasMaxExPoints = false;

    private bool attacking;

    void Start()
    {
        playerAgent = this.GetComponent<NavMeshAgent>();
        playerAnimator = this.GetComponent<Animator>();
        playerMovement = this.GetComponent<PlayerMovement>();
        playerCombat = this.GetComponent<Combat>();
        myStats = this.GetComponent<Stats>();
        addToVIT.onClick.AddListener(AddToVit);
        addToSTR.onClick.AddListener(AddtoStr);
    }

    void Update()
    {
        DisplayStats(myStats);
        HPBar.fillAmount = myStats.currentHP / 100;
        exBar.fillAmount = exPoints / 100;
        playerAnimator.SetBool("isAttacking", attacking);

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            statsPanel.SetActive(!statsPanel.activeSelf); 
        }

        //Right Mouse Button
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitPoint;

            if (Physics.Raycast(ray, out hitPoint, 100))
            {
                MovePlayer(hitPoint.point);
                RemoveTarget();
            }
        }

        //Left Mouse Button
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitPoint;

            if (Physics.Raycast(ray, out hitPoint, 100))
            {
                InteractableObj interactable = hitPoint.collider.GetComponent<InteractableObj>();

                if (interactable != null)
                {
                    SetTarget(interactable);
                    currentTarget = target.gameObject;

                    if (targetDistance <= playerAgent.stoppingDistance && currentTarget.gameObject.tag == "Enemy")
                    {
                        targetStats = currentTarget.GetComponent<Stats>();
                        attacking = true;
                        playerCombat.CharCombat(targetStats);
                    }
                }
            }
        }

        if (targetStats != null)
        {
            if (targetStats.currentHP <= 0)
            {
                acquireXP(targetStats);
                RemoveTarget();
                attacking = false;
            }
        }

        if (exPoints >= maxExPoints)
        {
            hasMaxExPoints = true;
            LevelUp(myStats);
            exPoints = 0;
        }

        if (myStats.currentHP <= 0)
        {
            Death();
        }
    }

    public void DisplayStats(Stats myStats)
    {
        gold.text = "Gold: " + myStats.gold;
        level.text = "Level: " + currentLevel;
        exp.text = "EXP: " + exPoints;
        attack.text = "ATK: " + myStats.currentDAM;
        vit.text = "VIT: " + myStats.VIT;
        str.text = "STR: " + myStats.STR;
        hp.text = "Health: " + myStats.currentHP;
        playerName.text = "Name: " + this.gameObject.name;
        points.text = "Unallocated Points: " + unallocatedPoints;
    }

    public void acquireXP(Stats targetStats)
    {
        switch (targetStats.gameObject.name)
        {
            case "Goblin (wmarker)":
                exPoints += 10;
                break;
            case "WildPig (wmarker)":
                exPoints += 5;
                break;
            case "Slime (wmarker)":
                exPoints += 2;
                break;
        }
    }

    public void LevelUp(Stats currentStats)
    {
        currentStats = myStats;

        if (hasMaxExPoints == true)
        {
            currentLevel++;
            unallocatedPoints += 3;
            hasMaxExPoints = false;
        }
    }

    public void AddToVit()
    {
        if (unallocatedPoints > 0)
        {
            unallocatedPoints--;
            myStats.VIT++;
            myStats.UpdateStats();
        }
    }

    public void AddtoStr()
    {
        if (unallocatedPoints > 0)
        {
            unallocatedPoints--;
            myStats.STR++;
            myStats.UpdateStats();
        }
    }

    public void MovePlayer(Vector3 targetPos)
    {
        playerMovement.MoveTo(targetPos);
    }

    public void SetTarget(InteractableObj newTarget)
    {
        targetDistance = Vector3.Distance(this.transform.position, newTarget.transform.position);
        target = newTarget;
        playerMovement.FollowTarget(newTarget);
        enemyScript = newTarget.gameObject.GetComponent<Enemy>();
        enemyScript.myMarker.SetActive(true);
        enemyScript.enemyHPBar.gameObject.SetActive(true);
    }

    public void RemoveTarget()
    {
        if (target != null)
        {
            enemyScript.enemyHPBar.gameObject.SetActive(false);
            enemyScript.myMarker.SetActive(false);
            target = null;
            targetStats = null;
            playerMovement.StopFollowing();
        }
    }

    public void Death()
    {
        Destroy(this.gameObject);
        Time.timeScale = 0;
    }

    public void StartEffect()
    {
    }

    public void StopEffect()
    {
    }
}
