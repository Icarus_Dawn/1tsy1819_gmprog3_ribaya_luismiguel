﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public int limit = 9;

    //singleton
    public static Inventory instance;
    public List<Item> items = new List<Item>();

    private void Awake()
    { 
        instance = this;
    }

    public delegate void OnInventoryChanged();
    public OnInventoryChanged onInventoryChangedCallback;

    public bool AddInventory(Item item)
    {
        if (items.Count > limit)
        {                 
            return false;
        }

        items.Add(item);

        if (onInventoryChangedCallback != null)
        {
            onInventoryChangedCallback.Invoke();
        }
        return true;
    }

    public void RemoveInventory(Item item)
    {
        items.Remove(item);

        if (onInventoryChangedCallback != null)
        {
            onInventoryChangedCallback.Invoke();
        }
    }
}
