﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldPickUp : InteractableObj
{
    private GameObject player;
    private Stats playerStats;
    public int goldAmt;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerStats = player.GetComponent<Stats>();
    }

    private void Update()
    {
        float distance = Vector3.Distance(player.transform.position, this.transform.position);
        if (distance <= interactRadius)
        {
            Interact();
        }
    }

    public override void Interact()
    {
        PickupGold();    
    }

    public void PickupGold()
    {
        playerStats.gold += goldAmt;
        Destroy(this.gameObject);
    }
}
