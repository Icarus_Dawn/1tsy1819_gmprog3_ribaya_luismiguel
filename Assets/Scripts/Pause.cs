﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public Button pause, resume, MainMenu;
    public RawImage Inventory;
    public bool isActive = false;

    void Start()
    {
        pause.onClick.AddListener(PauseGame);
        resume.onClick.AddListener(UnPause);
        MainMenu.onClick.AddListener(GoMainMenu);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void UnPause()
    {
        Time.timeScale = 1;
    }

    public void GoMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
