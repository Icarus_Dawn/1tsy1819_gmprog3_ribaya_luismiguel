﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsPickUp : InteractableObj
{
    private GameObject player;
    public Item itemType;
    
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        float distance = Vector3.Distance(player.transform.position, this.transform.position);
        if (distance <= interactRadius)
        {
            Interact();
        }
    }

    public override void Interact()
    {
        AcquireItem();
    }

    void AcquireItem()
    {
        bool pickedUp = Inventory.instance.AddInventory(itemType);

        if (pickedUp)
        {
            Debug.Log("Item Picked Up: " + itemType.name);
            Destroy(this.gameObject);
        }
    } 
}
