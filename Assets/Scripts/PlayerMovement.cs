﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    private Animator playerAnimator;

    public bool isRunning;
    public GameObject marker;
    public Transform playerTarget;

    NavMeshAgent playerNavMeshAgent;

    // Use this for initialization
    void Start ()
    {
        playerNavMeshAgent = GetComponent<NavMeshAgent>();
        playerAnimator = GetComponent<Animator>();
        playerNavMeshAgent = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (playerNavMeshAgent.remainingDistance <= playerNavMeshAgent.stoppingDistance)
        {
            marker.SetActive(false);
            isRunning = false;       
        }
        else
        {
            marker.SetActive(true);
            isRunning = true;
        }

        if (playerTarget != null)
        {
            this.transform.LookAt(playerTarget.position);
            playerNavMeshAgent.SetDestination(playerTarget.position);
        }

        playerAnimator.SetBool("isRunning", isRunning);
    }

    public void MoveTo(Vector3 point)
    {
        marker.transform.position = point + Vector3.up;
        playerNavMeshAgent.SetDestination(point);
    }

    public void FollowTarget(InteractableObj target)
    {
        playerNavMeshAgent.stoppingDistance = target.interactRadius;
        playerTarget = target.transform;
    }

    public void StopFollowing()
    {
        playerTarget = null;
    }
}
