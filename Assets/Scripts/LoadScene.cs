﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour
{
    public Button startGame, animDemo, back, exit;
    public Scene field;

    public void LoadGame()
    {
        SceneManager.LoadScene("3.Field");
    }

    public void LoadDemo()
    {
        SceneManager.LoadScene("Animations Scene");
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void Start()
    {
        startGame.onClick.AddListener(LoadGame);
        animDemo.onClick.AddListener(LoadDemo);
        back.onClick.AddListener(BackToMenu);
        exit.onClick.AddListener(ExitGame);
    }
}
