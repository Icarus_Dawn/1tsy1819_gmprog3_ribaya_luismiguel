﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUIController : MonoBehaviour
{
    public Transform gridManager;
    public GameObject UI;

    Inventory inventory;
    InventorySlotController[] slots;

	void Start ()
    {
        inventory = Inventory.instance;
        inventory.onInventoryChangedCallback += UiReferesh;
        slots = gridManager.GetComponentsInChildren<InventorySlotController>();
	}
	

	void Update ()
    {
        if (Input.GetKeyDown("i"))
        {
            UI.SetActive(!UI.activeSelf);
        }
	}

    void UiReferesh()
    {
        Debug.Log("UI REFRESHED");

        for (int i = 0; i < slots.Length; i++)
        {
            if (i < inventory.items.Count)
            {
                slots[i].AddNewItem(inventory.items[i]);
            }
            else
            {
                slots[i].clearItem();
            }
        }
    }
}
