﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Effect", menuName = "Inventory/Effect")]
public class Heal : Item
{
    public GameObject player;
    Stats myStats;

    // Use this for initialization
    void AddHealth()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        myStats = player.gameObject.GetComponent<Stats>();
        Debug.Log("HEALED");
        myStats.Heal(50);
    }

    public override void Use()
    {
        AddHealth();
        base.Use();      
    }
}
